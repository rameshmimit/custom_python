# python

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with python](#setup-requirements)
    * [Setup requirements](#setup-requirements)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Suggestions for future] (#Suggestions)

## Overview

This module will install custom python package,pip and  virtualenv

## Module Description

This will install custom python RPMS at /opt/python-$version location. /opt/python is symlink to /opt/python-$version for ease of use.
It also installs easy_install, pip and virtualenv at /opt/python/bin dir.

## Setup Requirements
* /opt directory must exit before running this module. It needs puppetlabs-stdlib module as well.
* Python custom RPMS must be present in your local hosted yum repo.
* RPMs can be build using fpm tool or traditional rpmbuild utility.


## Usage
include python
or
class { python: }

## Limitations
* It will always install python under /opt
* Must support multi os, it is tested only on centos-6.5 and centos-7.1
* Installation path should be configurable.
* Better documentation

## Suggestions

RPMs should never be a part of any puppet module or repo. Ideally they should be hosted on yum repo.

##### Note: 
I have created the RPMS using FPM. I am including Post install and uninstall scripts in this module itself.
For usage guide for FPM please read: https://github.com/jordansissel/fpm/wiki