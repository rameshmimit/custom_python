# == Class: python
#
# It will install custom python RPM under /opt/python folder.
#
# === Examples
#  include python
#
# or
# class { python: }
#
#
# === Authors
#
# Ramesh Kumar <ramesh.mimit@gmail.com>
#
#
#
class python (
  $version      = $python::params::version,
  $package_name = $python::params::package_name,
  $requirements = $python::params::requirements,
  $virtualenv_dir = $python::params::virtualenv_dir,
  $virtualenv_name = $python::params::virtualenv_name,

  ) inherits python::params {
$packages = [ 'wget','zlib-devel', 'bzip2-devel', 'openssl-devel', 'ncurses-devel', 'sqlite-devel', 'readline-devel','gdbm-devel', 'libpcap-devel', 'xz-devel', 'make', 'gcc','libxslt-devel', 'libxml2-devel', 'libffi-devel','libpng-devel','freetype-devel','gcc-c++','libtiff-devel','libjpeg-turbo-devel']
  $path    = '/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin'

  package { $packages:
    ensure => installed,
    before => Package["$::python::params::package_name"],
  }
  file { '/opt/python':
    ensure  => 'link',
    target  => "/opt/python-$version",
    require => Package["$::python::params::package_name"],
  }
  exec { 'install_pip':
    cwd     => '/opt/python',
    path    => $path,
    onlyif  => "test ! -f '/opt/python/bin/pip' ",
    command => 'wget https://bootstrap.pypa.io/get-pip.py && /opt/python/bin/python276 get-pip.py',
    creates => '/opt/python/bin/pip',
    require => [Package["$::python::params::package_name"],File['/opt/python']],
  }
  file { '/opt/python/bin/pip276':
    ensure  => 'link',
    target  => '/opt/python/bin/pip',
    require => Exec['install_pip'],
  }
  exec { 'easy_install':
    cwd     => '/opt/python',
    path    => $path,
    onlyif  => "test ! -f '/opt/python/bin/easy_install' ",
    command => 'wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py && /opt/python/bin/python276 ez_setup.py',
    creates => '/opt/python/bin/easy_install-2.7',
    require => [Package["$::python::params::package_name"],File['/opt/python']],
  }
  file { '/opt/python/bin/easy_install276':
    ensure  => 'link',
    target  => '/opt/python/bin/easy_install-2.7',
    require => Exec['easy_install'],
  }
  exec { 'install_virtualenv':
    cwd     => '/opt/python',
    path    => $path,
    onlyif  => "test ! -f '/opt/python/bin/virtualenv' ",
    command => '/opt/python/bin/pip276 install virtualenv',
    creates => '/opt/python/bin/virtualenv',
    require => [Exec['install_pip'],File['/opt/python']],
  }
  file { '/opt/python/bin/virtualenv276':
    ensure  => 'link',
    target  => '/opt/python/bin/virtualenv',
    require => Exec['install_virtualenv'],
  }
  file { '/etc/profile.d/python276.sh':
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    ensure  => present,
    content => 'export PATH=$PATH:/opt/python/bin',
    require => Package["$::python::params::package_name"],
  }
  file { $virtualenv_dir:
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  exec { 'initialize_virtualenv':
    cwd     => $virtualenv_dir,
    path    => $path,
    command => "/opt/python/bin/virtualenv $virtualenv_name",
    creates => "$virtualenv_dir/$virtualenv_name",
    require => [Exec['install_virtualenv'],File["$virtualenv_dir"]],
    onlyif  => "test ! -d $virtualenv_dir/$virtualenv_name",
  }
if $requirements {
  file { "$virtualenv_dir/requirements.txt":
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/python/requirements.txt'
  }
  exec { 'install_pip_modules_from_file':
    cwd       => "$virtualenv_dir",
    path      => $path,
    command   => "$virtualenv_dir/$virtualenv_name/bin/pip install -r $virtualenv_dir/requirements.txt",
    timeout   => $timeout,
    subscribe => File["$virtualenv_dir/requirements.txt"],
    require   => [Exec['initialize_virtualenv'],File["$virtualenv_dir/requirements.txt"]],
  }
 }
}
