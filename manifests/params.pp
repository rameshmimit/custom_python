class python::params (
  $version      = '2.7.6',
  $package_name = 'python276',
  $requirements = false,
 $virtualenv_dir = '/opt/virtualenv',
  $virtualenv_name = 'python276'
  ) {
  case $::architecture {
    'x86_64': {
      $rpm_name = 'python276-2.7.6-1.x86_64.rpm'
      file { "$rpm_name":
        path   => "/tmp/$rpm_name",
        owner  => 'root',
        group  => 'root',
        source => "puppet:///modules/python/rpms/$rpm_name",
      }
      package { "$package_name":
        provider        => 'rpm',
        install_options => ['-vh'],
        source          => "/tmp/$rpm_name",
        require         => File["$rpm_name"],
      }
    }
    default: {
      $rpm_name = 'python276-2.7.6-1.noarch.rpm'
      file { "$rpm_name":
        path   => "/tmp/$rpm_name",
        owner  => 'root',
        group  => 'root',
        source => "puppet:///modules/python/rpms/$rpm_name",
      }
      package { "$package_name":
        provider        => 'rpm',
        install_options => ['-ivh'],
        source          => "/tmp/$rpm_name",
        require         => File["$rpm_name"],
      }
    }
  }
}
